import pandas as pd
import pymysql
import json

df1 = pd.read_csv('/home/user/Downloads/startup.csv')
df2 = pd.read_parquet('/home/user/Downloads/consumerInternet.parquet')

# print(df1)
# print(df2)
with open('conn.json') as file:
    config = json.load(file)

db = pymysql.connect(host=config['host'],user=config['username'],password=config['password'],database=config['database'])

cursor = db.cursor()

result = df1.append(df2)
# print(result.dtypes)

result=result.fillna(0)

for index,row in result.iterrows():
    # print(row[0])
    # print(row[9])
    # break

    cursor.execute("insert into track1(Sr_No, Date, Startup_Name, Industry_Vertical, SubVertical, City, Investors_Name, InvestmentnType, Amount_in_USD, Remarks) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]))



print("completed")

db.commit()
cursor.close()
# print(frames)
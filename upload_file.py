import argparse
import pandas as pd
import pathlib
import json
import pymysql

parser = argparse.ArgumentParser("PythonToMysql")

parser.add_argument('-s','--source_dir',help='path to the directory')
parser.add_argument('-f','--mysql_details',help='path of mysql credential file')
parser.add_argument('-d','--destination_table',help='my table in mysql')
args = parser.parse_args()

with open(args.mysql_details) as file:
    config = json.load(file)

db = pymysql.connect(host=config['host'],user=config['username'],password=config['password'],database=config['database'])

cursor = db.cursor()

for file_insidedirectory in pathlib.Path(args.source_dir).iterdir():
    if file_insidedirectory.is_file():
        print(file_insidedirectory.name)
        try:
            df = pd.read_csv(file_insidedirectory)
            for index,row in df.iterrows():
                # print(row)
                # print(row[1])
                cursor.execute("insert into nihal(DEST_COUNTRY_NAME,ORIGIN_COUNTRY_NAME,count) values(%s,%s,%s)",(row[0],row[1],row[2]))
        except:
            print("hey! there is a file which is csv file or something is wrong")

print("completed")

db.commit()
cursor.close()

